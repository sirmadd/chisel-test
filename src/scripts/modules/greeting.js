import $ from 'jquery';
// import ScrollMagic from 'scrollmagic';

const greeting = name => {
  const element = $('.js-greeting');
  const select = $('.mySelect');

  // console.log('TEST');
  // console.log('Greet', ScrollMagic);

  if (element.length) {
    element.text(name);
  }

  if (select.length) {
    select.select2();
  }
};
export default greeting;
