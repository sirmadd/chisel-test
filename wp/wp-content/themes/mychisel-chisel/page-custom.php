<?php
/**
 * Template Name: Custom
 */

global $post;

$context = \Timber\Timber::get_context();

$context['testContext'] = 'Test string Custom template Context';

\Timber\Timber::render( array('page-custom.twig' ), $context );
